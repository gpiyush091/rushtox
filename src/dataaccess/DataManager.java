package dataaccess;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fileHandler.FileHandler;

public class DataManager {
	private final int TESTTYPE = 0;
	private final int RESULTTYPE = 1;
	private final int DISORDERTYPE = 2;
	
	private static DataManager _sharedInstance;
	
	private final int nodeDistanceInSameGroup = 40;
	
	private HashMap<String, JSONObject> tests = new HashMap<String, JSONObject>();
	private HashMap<String, JSONObject> results = new HashMap<String, JSONObject>();
	private HashMap<String, JSONObject> disorders = new HashMap<String, JSONObject>();
	
	private static int testNodeID = 0;
	private static int resultNodeID = 0;
	private static int disorderNodeID = 0;
	
	private TreeMap<String, ArrayList<String>> testResults = new TreeMap<>();
	private TreeMap<String, ArrayList<String>> resultTests = new TreeMap<>();
	private TreeMap<String, ArrayList<String>> testDisorders = new TreeMap<>();
	private TreeMap<String, ArrayList<String>> disorderTests = new TreeMap<>();
	private TreeMap<String, ArrayList<String>> disorderResults = new TreeMap<>();
	private TreeMap<String, ArrayList<String>> resultDisorders = new TreeMap<>();
	
	private JSONObject JSON = new JSONObject();
	private int maxY = 0;
	
	private HashMap<String, Integer> nodeMap = new HashMap<>();
	
	private JSONArray links = new JSONArray();
	private JSONArray nodes = new JSONArray();
	
	private void init() 
	{
		String dirPath = "F:\\remote\\CSE599\\TestData-Rush";
		System.out.println("The Game Begins !");
		FileHandler fh = new FileHandler();
		fh.readAllFiles(dirPath);
		System.out.println("The Game Ends !");
		
		// Populate all the data coming from file handler
		testResults.putAll(fh.testResults);
		resultTests.putAll(fh.resultTests);
		testDisorders.putAll(fh.testDisorders);
		disorderTests.putAll(fh.disorderTests);
		resultDisorders.putAll(fh.resultDisorders);
		disorderResults.putAll(fh.disorderResults);
		
		JSON = createLinks();
	}
	
	@SuppressWarnings("unchecked")
	private void setTests() {
		Iterator<Entry<String, ArrayList<String>>> it = testResults.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = it.next();
			JSONObject node = new JSONObject();
			node.put("id", Integer.toString(testNodeID));
			node.put("type", "t");
			node.put("text", pair.getKey());
			
			ArrayList<String> tdList = testDisorders.get(pair.getKey());
			JSONArray disordersJson = new JSONArray();
			for (int i=0;i<tdList.size();i++) {
				disordersJson.add(tdList.get(i));
			}
			node.put("disorders", disordersJson);
			
			JSONArray resultsJson = new JSONArray();
			// TreeMap<String, ArrayList<String>> temp = new TreeMap<>(resultDisorders);
			for (String part:pair.getValue()) {
				ArrayList<String> rdList = new ArrayList<String>(); 
				rdList.addAll(resultDisorders.get(part));
				rdList.retainAll(tdList);
				JSONArray resDis = new JSONArray();
				for (int k=0;k<rdList.size();k++) {
					resDis.add(rdList.get(k));
				}
				node.put(part, resDis);
				resultsJson.add(part);
			}
			node.put("results", resultsJson);
			
			tests.put(pair.getKey(), node);
			testNodeID++;
		}
		testNodeID = 0;
	}
	
	@SuppressWarnings("unchecked")
	private void setResults() {
		Iterator<Entry<String, ArrayList<String>>> it = resultTests.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = it.next();
			JSONObject node = new JSONObject();
			node.put("id", Integer.toString(resultNodeID));
			node.put("type", "r");
			node.put("text", pair.getKey());
			
			ArrayList<String> rdList = resultDisorders.get(pair.getKey());
			JSONArray disordersJson = new JSONArray();
			for (int i=0;i<rdList.size();i++) {
				disordersJson.add(rdList.get(i));
			}
			node.put("disorders", disordersJson);
			
			JSONArray testsJson = new JSONArray();
			for (String part:pair.getValue()) {
				testsJson.add(part);
			}
			node.put("tests", testsJson);
			results.put(pair.getKey(), node);
			resultNodeID++;
		}
		resultNodeID = 0;
	}
	
	@SuppressWarnings("unchecked")
	private void setDisorders() {
		Iterator<Entry<String, ArrayList<String>>> it = disorderResults.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = it.next();
			JSONObject node = new JSONObject();
			node.put("id", Integer.toString(disorderNodeID));
			node.put("type", "d");
			node.put("text", pair.getKey());
			
			ArrayList<String> dtList = disorderTests.get(pair.getKey());
			JSONArray testsJson = new JSONArray();
			for (int i=0;i<dtList.size();i++) {
				testsJson.add(dtList.get(i));
				ArrayList<String> trList = new ArrayList<>();
				trList.addAll(testResults.get(dtList.get(i)));
				ArrayList<String> drList = new ArrayList<>();
				drList.addAll(pair.getValue());
				drList.retainAll(trList);
				JSONArray trJSON = new JSONArray();
				for (int k=0;k<drList.size();k++) {
					trJSON.add(drList.get(k));
				}
				node.put(dtList.get(i), trJSON);
			}
			node.put("tests", testsJson);
			JSONArray drJson = new JSONArray();
			for (int m=0;m<pair.getValue().size();m++) {
				drJson.add(pair.getValue().get(m));
			}
			node.put("results", drJson);
			disorders.put(pair.getKey(), node);
			disorderNodeID++;
		}
		disorderNodeID = 0;
	}
	
	private JSONObject createLinks() {
		if (tests.isEmpty()) {
			setTests();
		}
		if (results.isEmpty()) {
			setResults();
		}
		if (disorders.isEmpty()) {
			setDisorders();
		}
		
		Integer nodeID = 0;
		
		JSONObject json = new JSONObject();
		
		
		int y=500;
		// Results is the smallest hash map so parse it to get all nodes
		Iterator<Entry<String, JSONObject>> rx = results.entrySet().iterator();
		while (rx.hasNext()) {
			Map.Entry<String, JSONObject> pair = rx.next();
			JSONObject testNode = new JSONObject();
			testNode.put("name", pair.getKey());
			testNode.put("group", RESULTTYPE);
			testNode.put("x", 400);
			testNode.put("y", y);
			testNode.put("fixed", true);
			nodes.add(testNode);
			y = y+nodeDistanceInSameGroup + 40;
			
			nodeMap.put(pair.getKey(), nodeID);
			nodeID++;
		}
		y = y-500;
		if (y>maxY) {
			maxY = y;
		}
		
		y = 10;
		Iterator<Entry<String, JSONObject>> dx = disorders.entrySet().iterator();
		while (dx.hasNext()) {
			Map.Entry<String, JSONObject> pair = dx.next();
			JSONObject testNode = new JSONObject();
			testNode.put("name", pair.getKey());
			testNode.put("group", DISORDERTYPE);
			testNode.put("x", 600);
			testNode.put("y", y);
			testNode.put("fixed", true);
			nodes.add(testNode);
			y = y+nodeDistanceInSameGroup;
			
			nodeMap.put(pair.getKey(), nodeID);
			nodeID++;
		}
		
		if (y>maxY) {
			maxY = y;
		}
		
		// Parse the tests map to generate links
		y = 100;
		Iterator<Entry<String, JSONObject>> tx = tests.entrySet().iterator();
		while (tx.hasNext()) {
			Map.Entry<String, JSONObject> pair = tx.next();
			JSONObject testNode = new JSONObject();
			testNode.put("name", pair.getKey());
			testNode.put("group", TESTTYPE);
			testNode.put("x", 10);
			testNode.put("y", y);
			testNode.put("fixed", true);
			nodes.add(testNode);
			
			nodeMap.put(pair.getKey(), nodeID);
			nodeID++;
			
			y = y+nodeDistanceInSameGroup;
		}
		y = y-100;
		
		if (y>maxY) {
			maxY = y;
		}
		
		Iterator<Entry<String, ArrayList<String>>> tc = testResults.entrySet().iterator();
		while (tc.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = tc.next();
			ArrayList<String> resList = new ArrayList<>(); 
			resList.addAll(pair.getValue());
			
			for (String res: resList) {
				JSONObject link = new JSONObject();
				link.put("source", nodeMap.get(pair.getKey()));
				link.put("target", nodeMap.get(res));
				link.put("value", 1);
				link.put("stroke", "gray");
				links.add(link);
			}
		}
		
		tc = testDisorders.entrySet().iterator();
		while (tc.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = tc.next();
			ArrayList<String> resList = new ArrayList<>(); 
			resList.addAll(pair.getValue());
			
			for (String res: resList) {
				JSONObject link = new JSONObject();
				link.put("source", nodeMap.get(pair.getKey()));
				link.put("target", nodeMap.get(res));
				link.put("value", 1);
				link.put("stroke", "pink");
				links.add(link);
			}
		}
		
		tc = resultDisorders.entrySet().iterator();
		while (tc.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = tc.next();
			ArrayList<String> resList = new ArrayList<>(); 
			resList.addAll(pair.getValue());
			
			for (String res: resList) {
				JSONObject link = new JSONObject();
				link.put("source", nodeMap.get(pair.getKey()));
				link.put("target", nodeMap.get(res));
				link.put("value", 1);
				link.put("stroke", "blue");
				links.add(link);
			}
		}
		
		tc = resultTests.entrySet().iterator();
		while (tc.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = tc.next();
			ArrayList<String> resList = new ArrayList<>(); 
			resList.addAll(pair.getValue());
			
			for (String res: resList) {
				JSONObject link = new JSONObject();
				link.put("source", nodeMap.get(pair.getKey()));
				link.put("target", nodeMap.get(res));
				link.put("value", 1);
				link.put("stroke", "gray");
				links.add(link);
			}
		}
		
		tc = disorderTests.entrySet().iterator();
		while (tc.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = tc.next();
			ArrayList<String> resList = new ArrayList<>(); 
			resList.addAll(pair.getValue());
			
			for (String res: resList) {
				JSONObject link = new JSONObject();
				link.put("source", nodeMap.get(pair.getKey()));
				link.put("target", nodeMap.get(res));
				link.put("value", 1);
				link.put("stroke", "pink");
				links.add(link);
			}
		}
	
		tc = disorderResults.entrySet().iterator();
		while (tc.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = tc.next();
			ArrayList<String> resList = new ArrayList<>(); 
			resList.addAll(pair.getValue());
			
			for (String res: resList) {
				JSONObject link = new JSONObject();
				link.put("source", nodeMap.get(pair.getKey()));
				link.put("target", nodeMap.get(res));
				link.put("value", 1);
				link.put("stroke", "blue");
				links.add(link);
			}
		}
		
		json.put("nodes", nodes);
		json.put("links", links);
		return json;
	}
	
	
	
	private JSONArray createLinksJSON(FileHandler fh) {
		int count=0;
		JSONArray json = new JSONArray();
		TreeMap<String, ArrayList<String>> tm = fh.testResults;
		Iterator it = tm.entrySet().iterator();
		
		while(it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = (Map.Entry<String, ArrayList<String>>) it.next();
			ArrayList<String> parts = pair.getValue();
			for (String part : parts) {
				JSONObject segment = new JSONObject();
				segment.put("source", pair.getKey().toString());
				segment.put("target", part);
				json.add(segment);
			}
			count++;
			if (count == 10) {
				break;
			}
		}
		
		count = 0;
		tm = fh.disorderResults;
		it = tm.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = (Map.Entry<String, ArrayList<String>>) it.next();
			ArrayList<String> parts = pair.getValue();
			for (String part : parts) {
				JSONObject segment = new JSONObject();
				segment.put("source", pair.getKey().toString());
				segment.put("target", part);
				json.add(segment);
			}
			count++;
			if (count == 10) {
				break;
			}
		}
		
		count = 0;
		tm = fh.testDisorders;
		it = tm.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = (Map.Entry<String, ArrayList<String>>) it.next();
			ArrayList<String> parts = pair.getValue();
			for (String part : parts) {
				JSONObject segment = new JSONObject();
				segment.put("source", pair.getKey().toString());
				segment.put("target", part);
				json.add(segment);
			}
			count++;
			if (count == 10) {
				break;
			}
		}
		return json;
	}
	
	public static synchronized DataManager getInstance() throws Exception {
		if (_sharedInstance == null) {
			_sharedInstance = new DataManager();
			_sharedInstance.init();
		}
		return _sharedInstance;
	}
	
	public Collection<JSONObject> getTests() {
		if (tests.isEmpty()) {
			setTests();
		}
		return tests.values();
	}
	
	public Collection<JSONObject> getResults() {
		if (results.isEmpty()) {
			setResults();
		}
		return results.values();
	}
	
	public Collection<JSONObject> getDisorders() {
		if (disorders.isEmpty()) {
			setDisorders();
		}
		return disorders.values();
	}
	
	public JSONObject getLinksForGraph() {
		return JSON;
	}
	
	public int getHeight() {
		// 100 is for padding
		return maxY+100;
	}
	
	private JSONObject getTest(String test) {
		JSONObject json = new JSONObject();
		JSONObject testJSON = tests.get(test);
		JSONArray resArr = new JSONArray();
		resArr = (JSONArray) testJSON.get("results");
		JSONArray disArr = new JSONArray();
		disArr = (JSONArray) testJSON.get("disorders");
		
		JSONArray subsetNodes = new JSONArray();
		JSONArray subsetLinks = new JSONArray();
		
		JSONObject testNode = new JSONObject();
		testNode.put("name", test);
		testNode.put("group", TESTTYPE);
		testNode.put("x", 40);
		testNode.put("y", 50);
		testNode.put("fixed", true);
		subsetNodes.add(testNode);
		
		// get nodes by iterating from node JSONArray
		int y = 20;
		for (int i=0;i<resArr.size();i++) {
			JSONObject resNode = new JSONObject();
			resNode.put("name", resArr.get(i));
			resNode.put("group", RESULTTYPE);
			resNode.put("x", 400);
			resNode.put("y", y);
			resNode.put("fixed", true);
			subsetNodes.add(resNode);
			y = y+nodeDistanceInSameGroup;
		}
		
		y = 20;
		for (int i=0;i<disArr.size();i++) {
			JSONObject disNode = new JSONObject();
			disNode.put("name", disArr.get(i));
			disNode.put("group", DISORDERTYPE);
			disNode.put("x", 800);
			disNode.put("y", y);
			disNode.put("fixed", true);
			subsetNodes.add(disNode);
			y = y+nodeDistanceInSameGroup;
		}
		
		// get links from testDisorders
		ArrayList<String> disList = new ArrayList();
		disList = testDisorders.get(test);
		for (int j=0;j<disList.size();j++) {
			// Create link and break
			JSONObject link = new JSONObject();
			link.put("source", nodeMap.get(test));
			link.put("target", nodeMap.get(disList.get(j)));
			link.put("value", 1);
			link.put("stroke", "pink");
			subsetLinks.add(link);
		}
		
		// testResults
		ArrayList<String> resList = new ArrayList();
		resList = resultTests.get(test);
		for (int j=0;j<resList.size();j++) {
			// Create link and break
			JSONObject link = new JSONObject();
			link.put("source", nodeMap.get(test));
			link.put("target", nodeMap.get(resList.get(j)));
			link.put("value", 1);
			link.put("stroke", "blue");
			subsetLinks.add(link);
		}
		
		// disorderResults
		for (int i=0;i<disArr.size();i++) {
			ArrayList<String> resultList = new ArrayList<String>();
			resultList = disorderResults.get(disArr.get(i));
			for (int j=0;j<resArr.size();j++) {
				for (int k=0;k<resultList.size();k++) {
					if(resArr.get(j).equals(resultList.get(k))) {
						JSONObject link = new JSONObject();
						link.put("source", nodeMap.get(disArr.get(i)));
						link.put("target", nodeMap.get(resultList.get(k)));
						link.put("value", 1);
						link.put("stroke", "gray");
						subsetLinks.add(link);
						break;
					}
				}
			}
		}
		
		json.put("nodes", subsetNodes);
		json.put("links", subsetLinks);
		
		return json;
	}
	
	private JSONObject getResult(String result) {
		return null;
	}
	
	private JSONObject getDisorder(String test) {
		return null;
	}
	
	public JSONObject getSubset(String node, int group) 
	{
		JSONObject linksAndNodes = new JSONObject();
		switch(group) {
			case 0:
				linksAndNodes = getTest(node); 
				break;
			case 1:
				linksAndNodes = getResult(node);
				break;
			case 2:
				linksAndNodes = getDisorder(node);
				break;
			default:
				System.out.println("Undefined");
				break;
		}
		return null;
	}
	
}