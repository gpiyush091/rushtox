package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fileHandler.FileHandler;

/**
 * Servlet implementation class RushToxApi
 */
@WebServlet("/RushToxApi")
public class RushToxApi extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RushToxApi() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String dirPath = "F:\\remote\\CSE599\\TestData-Rush";
		System.out.println("The Game Begins !");
		FileHandler fh = new FileHandler();
		fh.readAllFiles(dirPath);
		System.out.println("The Game Ends !");
		
		// Create json
		JSONArray json = createJSON(fh);
		request.setAttribute("links", json);
				
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private JSONArray createJSON(FileHandler fh) {
		int count=0;
		JSONArray json = new JSONArray();
		TreeMap<String, ArrayList<String>> tm = fh.testResults;
		Iterator it = tm.entrySet().iterator();
		
		while(it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = (Map.Entry<String, ArrayList<String>>) it.next();
			ArrayList<String> parts = pair.getValue();
			for (String part : parts) {
				JSONObject segment = new JSONObject();
				segment.put("source", pair.getKey().toString());
				segment.put("target", part);
				json.add(segment);
			}
			count++;
			if (count == 10) {
				break;
			}
		}
		
		count = 0;
		tm = fh.disorderResults;
		it = tm.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = (Map.Entry<String, ArrayList<String>>) it.next();
			ArrayList<String> parts = pair.getValue();
			for (String part : parts) {
				JSONObject segment = new JSONObject();
				segment.put("source", pair.getKey().toString());
				segment.put("target", part);
				json.add(segment);
			}
			count++;
			if (count == 10) {
				break;
			}
		}
		
		count = 0;
		tm = fh.testDisorders;
		it = tm.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = (Map.Entry<String, ArrayList<String>>) it.next();
			ArrayList<String> parts = pair.getValue();
			for (String part : parts) {
				JSONObject segment = new JSONObject();
				segment.put("source", pair.getKey().toString());
				segment.put("target", part);
				json.add(segment);
			}
			count++;
			if (count == 10) {
				break;
			}
		}
		return json;
	}
}
