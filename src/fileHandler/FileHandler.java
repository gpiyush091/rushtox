package fileHandler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileHandler {
	private final String startTitleDOM = "<h2 class=\"adamTitle\">";
	private final String endTitleDOM = "</h2>";
	private final String startAbnormalDOM = "<h3>What abnormal results mean</h3>";
	private final String[] endAbnormalDOM = {"<h3>What the risks are</h3>","<div class=\"reviewInfo\">",
			"<h3>References</h3>","<h3>Special considerations</h3>"};

	private final String[] startDisordersDOM = {"<ul class=\"prgul\">"};
	private final String[] endDisorderDOM = {"</ul><p>","</ul>","."};

	private final String paraStartDOM = "<p>";
	//private final String paraEndDOM = "</p>";
	private final String paraEndListStartDOM = "</p><ul class=\"prgul\">";

	private final Pattern testnameTemplate = Pattern.compile(".*?<h4>.*?<\\/h4>.*?",Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	
	private final Pattern[] ResultTemplates = {
			Pattern.compile(".*?increase.*?decrease.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?decrease.*?increase.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?high.*?low.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?low.*?high.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?increase.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?high.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?greater.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?elevated.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?above.*?normal.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?decrease.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?low.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?false positive.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?positive.*?", Pattern.CASE_INSENSITIVE),
			Pattern.compile(".*?negative.*?", Pattern.CASE_INSENSITIVE)
	};
	
	
	private final Pattern[] DisorderTemplates = {
			Pattern.compile(paraStartDOM+".*?may be due.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may also be due to.*?"+paraEndListStartDOM), // because of 003403
			Pattern.compile(paraStartDOM+".*?can result from.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?conditions under which.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may suggest.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be seen in.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be a result of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be a sign of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?following.*?problem.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may reveal.*?following.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may mean.*?"+paraEndListStartDOM),
			//Pattern.compile(paraStartDOM+".*?may be caused by.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?caused by.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may indicate.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be related to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?could be due to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?causes.*?include.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?is a sign of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?condition.*?cause.*?"+paraEndListStartDOM),// NO following
			Pattern.compile(paraStartDOM+".*?may be present because.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?conditions.*?affect.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?problems.*?affect.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?list of causes.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may also be done to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may include.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?can be a sign of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be done to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?occur in persons with.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may also occur with.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?conditions for which the test may be performed.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may occur due to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?following conditions.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may also increase after.*?"+paraEndListStartDOM), // special case only occurs in 003472
			//Pattern.compile(paraStartDOM+".*?can be caused by.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Higher-than-normal.*?"+paraEndListStartDOM), // special case 003497
			Pattern.compile(paraStartDOM+".*?Lower-than-normal.*?"+paraEndListStartDOM), // special case 003497
			Pattern.compile(paraStartDOM+".*?Additional conditions.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Conditions that can.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?conditions may cause.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Increased.*?"+paraEndListStartDOM), // 3535
			Pattern.compile(paraStartDOM+".*?Decreased.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?following diseases.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may occur with.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?greater-than-average risk for.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?High levels of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?test may mean.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?normal values indicate.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?test may also show.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?levels usually indicate.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?following may decrease.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?following may increase.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?disorders include.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?less common autoimmune disorders, including.*?"+paraEndListStartDOM), // 3638 only
			Pattern.compile(paraStartDOM+".*?problems such as.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?other conditions.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?can also develop due to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?test is helpful in determining if you have.*?"+paraEndListStartDOM), //003696
			Pattern.compile(paraStartDOM+".*?Causes of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Disorders that may be associated.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?test may also be performed for.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Lower than normal.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may help diagnose.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Abnormal results show that the stain is positive for.*?"+paraEndListStartDOM), // 003724
			Pattern.compile(paraStartDOM+".*?Culture can detect.*?"+paraEndListStartDOM),  //003754
			Pattern.compile(paraStartDOM+".*?infections caused.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?conditions such as.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may show.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?The abdominal CT scan may.*?"+paraEndListStartDOM), // 003789 special case because of a special character
			Pattern.compile(paraStartDOM+".*?may be used to identify.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?test can detect.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be performed for.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?problems of the small intestine.*?"+paraEndListStartDOM), // 003818 only
			Pattern.compile(paraStartDOM+".*?indicates disorders.*?"+paraEndListStartDOM), // 003823 only
			Pattern.compile(paraStartDOM+".*?conditions that may be discovered.*?"+paraEndListStartDOM), // 003856 only
			Pattern.compile(paraStartDOM+".*?Many disorders can be diagnosed with bronchoscopy, including.*?"+paraEndListStartDOM), //003857 only
			Pattern.compile(paraStartDOM+".*?diseases that may affect.*?"+paraEndListStartDOM), //003879 only
			Pattern.compile(paraStartDOM+".*?may detect many diseases.*?"+paraEndListStartDOM), //003880 only
			Pattern.compile(paraStartDOM+".*?can indicate.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be the result of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be signs of.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?may be also performed for.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Other causes.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?anything abnormal was seen during the test, including.*?"+paraEndListStartDOM), // 003913 only
			Pattern.compile(paraStartDOM+".*?infections are due to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?Benign bone tumors include.*?"+paraEndListStartDOM), // 003923 only 
			Pattern.compile(paraStartDOM+".*?Cancerous tumors include.*?"+paraEndListStartDOM), // 003923 only
			Pattern.compile(paraStartDOM+".*?results are due to.*?"+paraEndListStartDOM),
			Pattern.compile(paraStartDOM+".*?reasons for an abnormal result.*?"+paraEndListStartDOM)
	};
	
	

	TreeMap<String, String> hm = new TreeMap<>();
	TreeMap<String, String> intermediate = new TreeMap<>();

	TreeMap<String, String> resultsIntermediate = new TreeMap<>();
	
	public TreeMap<String, ArrayList<String>> testResults = new TreeMap<>();
	public TreeMap<String, ArrayList<String>> resultTests = new TreeMap<>();
	public TreeMap<String, ArrayList<String>> testDisorders = new TreeMap<>();
	public TreeMap<String, ArrayList<String>> disorderTests = new TreeMap<>();
	public TreeMap<String, ArrayList<String>> disorderResults = new TreeMap<>();
	public TreeMap<String, ArrayList<String>> resultDisorders = new TreeMap<>();

	private static int count = 0;
	public void readAllFiles(String dirPath) {

		File folder = new File(dirPath);
		if (folder.isDirectory()) {
			File[] files = folder.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File folder, String fileName) {
					return fileName.endsWith(".html");
				}

			});

			/* Commented as it was used for debugging purposes */
			// String filename = "F:\\remote\\CSE599\\TestData-Rush\\rush-003690.html";

			for (int i=0; i<files.length; i++) {
				// if (filename.equals(files[i].toString())){
					parseHTML(files[i]);
					// break;
				// }
			}	
		}
		System.out.println(count);

		separateAbnormalResultsAndDisorders();
		writeResult(intermediate, "F:\\remote\\CSE599\\test\\data-tests-disorders.html");
		writeResult(resultsIntermediate, "F:\\remote\\CSE599\\test\\results-intermediate.txt");
		
		/*Tree maps have been populated here.*/
		writeResultTreeMapArrayList(resultDisorders, "F:\\remote\\CSE599\\test\\result-disorders.html", "result-disorders map");
		writeResultTreeMapArrayList(testDisorders, "F:\\remote\\CSE599\\test\\test-disorders.html", "test-disorders map");
		writeResultTreeMapArrayList(testResults, "F:\\remote\\CSE599\\test\\test-results.html", "test-results map");
		
		reverseTreeMapArrayList(resultDisorders, "resultDisorders");
		reverseTreeMapArrayList(testDisorders, "testDisorders");
		reverseTreeMapArrayList(testResults, "testResults");
		
		writeResultTreeMapArrayList(disorderResults, "F:\\remote\\CSE599\\test\\disorder-results.html", "disorder-results map");
		writeResultTreeMapArrayList(disorderTests, "F:\\remote\\CSE599\\test\\disorder-tests.html", "disorder-tests map");
		writeResultTreeMapArrayList(resultTests, "F:\\remote\\CSE599\\test\\result-tests.html", "result-tests map");
	}
	
	private void reverseTreeMapArrayList(TreeMap<String, ArrayList<String>> tmpHm, String identifier) {
		TreeMap<String, ArrayList<String>> reverseMap = new TreeMap<>();
		Iterator it = tmpHm.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, ArrayList<String>> pair = (Map.Entry<String, ArrayList<String>>) it.next();
			ArrayList<String> parts = pair.getValue(); 
			for (String part:parts) {
				String key = part;
				
				Iterator itx = tmpHm.entrySet().iterator();
				while (itx.hasNext()) {
					Map.Entry<String, ArrayList<String>> tmpPair = (Map.Entry<String, ArrayList<String>>) itx.next();
					
					if (tmpPair.getValue().contains(key)) {
						if (reverseMap.containsKey(key)) {
							if (!reverseMap.get(key).contains(tmpPair.getKey())){
								reverseMap.get(key).add(tmpPair.getKey());
							}
						} else {
							ArrayList<String> tempArr = new ArrayList<>();
							tempArr.add(tmpPair.getKey());
							reverseMap.put(key, tempArr);
						}	
					}	
				}
			}
		}
		
		if (identifier.equals("resultDisorders")){
			disorderResults.putAll(reverseMap);
		} else if (identifier.equals("testDisorders")) {
			disorderTests.putAll(reverseMap);
		} else if (identifier.equals("testResults")) {
			resultTests.putAll(reverseMap);
		} else {
			System.out.println("Do not recognize the identifier");
		}
	}

	private void separateAbnormalResultsAndDisorders() 
	{
		//All the abnormal values in the TreeMap hm
		Iterator it = hm.entrySet().iterator();

		Boolean flagIfNoPara = false;
		while (it.hasNext()) {
			Map.Entry<String, String> pairs = (Entry<String, String>) it.next(); 
			ArrayList<String> results = new ArrayList<>();
			ArrayList<String> disorders = new ArrayList<>();
			
			String abnormal = pairs.getValue();
			int i=-1;
			// We do not need to compare the full abnormal but just the para before <ul> starts.

			String txt = abnormal;

			// TestName
			String testName = abnormal;
			Matcher testnameMatcher = testnameTemplate.matcher(abnormal);
			if (testnameMatcher.find()) {
				String parts[] = testName.split("</h4>");
				testName = parts[0];
				testName = testName.replaceAll("<.*?>", "");
			}
			
			while (txt.indexOf(startDisordersDOM[0])>=0) {
				int endIndex = txt.indexOf(paraEndListStartDOM);
				if (endIndex < 0) {
					flagIfNoPara = true;
					break;
				}
				
				String tmp = txt.substring(0, endIndex);
				int startIndex = tmp.lastIndexOf(paraStartDOM);
				txt = txt.substring(startIndex);
				
				for (i=0;i<DisorderTemplates.length;i++) {
					Matcher matcher = DisorderTemplates[i].matcher(txt);
					
					while (matcher.find()) {
						// Test result
						getResultBasedOnDOM(pairs.getKey(), txt, i, matcher, results, disorders);
					}	
				}
				
				startIndex = txt.indexOf("</ul>");
				txt = txt.substring(startIndex);
			}

			if (flagIfNoPara) {
				flagIfNoPara = false;
			}
			
			if (!disorders.isEmpty()) {
				String rd = results.toString().concat(disorders.toString());
				intermediate.put(pairs.getKey(),rd);	
			}
			
			// Put the hash map code here
			// Populate test results map
			ArrayList<String> tmpResults = new ArrayList<>();
			for (int m=0; m<results.size();m++) {
				String res = cleanResults(results.get(m));
				tmpResults.add(res);
			}
			
			// Eliminate duplicates
			HashSet<String> tmpResColl = new HashSet<>(tmpResults);
			tmpResults.removeAll(tmpResults);
			tmpResults.addAll(tmpResColl);
			
			if (testResults.containsKey(testName)) {
				Collection<String> resColl = testResults.get(testName);
				boolean isSubSet = tmpResults.containsAll(resColl);
				if (!isSubSet) {
					resColl.addAll(tmpResults);
					tmpResColl.removeAll(tmpResColl);
					tmpResColl.addAll(resColl);
					resColl.removeAll(resColl);
					resColl.addAll(tmpResColl);
					testResults.put(testName, (ArrayList<String>) resColl);
				}
			} else {
				if (!tmpResults.isEmpty()) {
					testResults.put(testName, tmpResults);	
				}
			}
			// System.out.println(results);
			for (int k=0; k<results.size(); k++) {
				String r = results.get(k);
				String result = cleanResults(r);
				ArrayList<String> disorderSetForAResult = cleanDisorder(disorders.get(k), result);
				// Eliminate duplicates
				HashSet<String> uniqueDisorders = new HashSet<>(disorderSetForAResult);
				disorderSetForAResult.removeAll(disorderSetForAResult);
				disorderSetForAResult.addAll(uniqueDisorders);
				
				// Populate test disorders map
				if (testDisorders.containsKey(testName)) {
					Collection<String> disColl = testDisorders.get(testName);
					boolean isSubSet = disorderSetForAResult.containsAll(disColl);
					if (!isSubSet) {
						disColl.addAll(disorderSetForAResult);
						uniqueDisorders.removeAll(uniqueDisorders);
						uniqueDisorders.addAll(disColl);
						disColl.removeAll(disColl);
						disColl.addAll(uniqueDisorders);
						testDisorders.put(testName, (ArrayList<String>) disColl);
					}
				} else { 
					if (!disorderSetForAResult.isEmpty()) {
						testDisorders.put(testName, disorderSetForAResult);	
					}
				}
				
				// Populate results disorders map
				disorderSetForAResult = cleanDisorder(disorders.get(k), result);
				
				if (resultDisorders.containsKey(result)) {
					Collection<String> disorderColl = resultDisorders.get(result);
					boolean isSubset = disorderSetForAResult.containsAll(disorderColl);
					if (!isSubset) {
						disorderColl.addAll(disorderSetForAResult);
						uniqueDisorders.removeAll(uniqueDisorders);
						uniqueDisorders.addAll(disorderColl);
						disorderColl.removeAll(disorderColl);
						disorderColl.addAll(uniqueDisorders);
						resultDisorders.put(result, (ArrayList<String>) disorderColl);
					}
				} else {
					if (!disorderSetForAResult.isEmpty()) {
						resultDisorders.put(result,disorderSetForAResult);	
					}
				}
			}
		}
	}
	
	private ArrayList<String> cleanDisorder(String disorders, String result) {
		String[] parts = disorders.split("</li><li>");
		
		ArrayList<String> disorderSet = new ArrayList<>();
		int c = 0;
		for (String part:parts) {
			part = part.replaceAll(".*?<ul class=\"prgul\"><li>", "");
			part = part.replace("<ul><li>", "");
			part = part.replace("</li></ul>", "");
			part = part.replaceAll("<a href=\".*?\">", "");
			part = part.replace("</a>", "");
			// Replace comma with underscore to remove confusion in reading the arraylist in file.
			part = part.replaceAll(",", "_");
			part = part.replaceAll("<.*?>", "");
			part = part.concat("("+result+")");
			disorderSet.add(c, part);
			c++;
		}
		return disorderSet;
	}

	private void getResultBasedOnDOM(String title, String abnormal, int i, 
			Matcher matcher, ArrayList<String> results, ArrayList<String> disorders) 
	{
		int startIndex = -1;
		String result = null;
		
		try {
			String tmp1 = abnormal.substring(matcher.start()+paraStartDOM.length(), matcher.end()-paraEndListStartDOM.length());
			startIndex = tmp1.lastIndexOf(paraStartDOM);
			
			if (startIndex >= 0){
				result = tmp1.substring(startIndex+paraStartDOM.length());
			} else {
				result = tmp1;
			}
			
		} catch (StringIndexOutOfBoundsException e) {
			// Way to go CHUNKER !!!!
		}
		if (result == null) {
			return;
		}

		/*Clean results*/
		//result = cleanResults(result);
		
		/* Disorder for the test */
		String disorder = getDisordersBasedOnDOM(abnormal.substring(matcher.end()-startDisordersDOM[0].length()), i);
		if (disorder.compareTo("<ul></ul>") == 0 ) {
			// Do nothing - no disorders
		} else {
			result = cleanResultsUsingChunker(result);
			results.add(result);
			resultsIntermediate.put(result, "1");
			disorders.add(disorder);
		}
	}
	
	private String cleanResults(String result) {
		result = result.replaceAll("<.*?>", "");
		result = result.replace(":", "");
		
		/*Categorize test results based on high/low/positive/negative/abnormal results*/
		result = getResultsKeyword(result);
		return result;
	}

	private String getResultsKeyword(String result) {
		boolean flag = false;
		for (int i=0;i<ResultTemplates.length;i++) {
			Matcher matcher = ResultTemplates[i].matcher(result);
			while (matcher.find()) {
				// Test result
				switch(i) {
					case 0:// increase decrease in same sentence
					case 1:// decrease increase in same sentence
					case 2:// high low in same sentence
					case 3:// low high in same sentence
						flag = true;
						result = "abnormal result";//(increase/decrease, high/low in same sentence)";
						break;
					case 4:// increase
					case 5:// high
					case 6:// greater
					case 7:// elevated
					case 8:// above.*?normal
						flag = true;
						result = "increase";// (increase.*?/high.*?/greater.*?/elevated.*?/above.*?normal)";
						break;
					case 9:// decrease
					case 10:// low
						flag = true;
						result = "decrease";// (decrease.*?/low.*?)";
						break;
					case 11:// false positive
						flag = true;
						result = "false positive";
						break;
					case 12:// positive
						flag = true;
						result = "positive";
						break;
					case 13:// negative
						flag = true;
						result = "negative";
						break;
					default:
						break;
				}
			}
			if (flag) {
				break;
			}
		}
		if (!flag) {
			result = "abnormal (rest)";
		}
		return result;
	}
	
	private String cleanResultsUsingChunker(String result) {
		// Parse using the chunker .. handle the case like 
		// increased level may be a sign of _____________. Decreased levels may be due to
		
		return result;
	}

	private String getDisordersBasedOnDOM(String abnormal, int i) {
		String disorder = "<ul>";
		switch (i) {
		case 0: // may be due to 
		case 1:	// may also be due to
		case 2: // can result from
			//case 2: // conditions may affect
		case 3: // conditions under which
		case 4: // may suggest
		case 5: // may be seen in
		case 6: // may be a result of
		case 7: // may be a sign of
			//case 8: // conditions may cause
		case 8: // problem
		case 9: // may reveal
		case 10: // may mean
			//case 11: // may be caused by
		case 11: // caused by
		case 12: // may indicate
		case 13: // may be related to
		case 14: // could be due to
			//case 15: // possible causes include
		case 15: // causes include
			//case 18: // conditions that cause
		case 16: // is a sign of
		case 17: // conditions * cause
		case 18: // may be present because
		case 19: // conditions * affect
		case 20: // problems *affect
		case 21: // list of causes
		case 22: // may also be done to
		case 23: // may include
		case 24: // can be a sign of
		case 25: // may be done to
		case 26: // occur in persons with
		case 27: // may also occur with
		case 28: // conditions for which the test may be performed
		case 29: // may occur due to
		case 30: // following conditions
		case 31: //  may also increase after
			//case 32: // can be caused by
		case 32: // Higher-than-normal
		case 33: // Lower-than-normal
		case 34: // Additional conditions
		case 35: // Conditions that can
		case 36: // conditions may cause
		case 37: // Increased
		case 38: // Decreased
		case 39: // following diseases
		case 40: // may occur with
		case 41: // greater-than-average risk for
		case 42: // High levels of
		case 43: // test may mean
		case 44: // normal values indicate
		case 45: // test may also show
		case 46: // levels usually indicate
		case 47: // following may decrease
		case 48: // following may increase
		case 49: // disorders include
		case 50: // less common autoimmune disorders, including
		case 51: // problems such as
		case 52: // other conditions
		case 53: // can also develop due to
		case 54: // test is helpful in determining if you have
		case 55: // Causes of 
		case 56: // Disorders that may be associated
		case 57: // test may also be performed for
		case 58: // Lower than normal
		case 59: // may help diagnose
		case 60: // Abnormal results show that the stain is positive for
		case 61: // Culture can detect
		case 62: // infections caused
		case 63: // conditions such as
		case 64: // may show
		case 65: // The abdominal CT scan may
		case 66: // may be used to identify
		case 67: // test can detect
		case 68: // may be performed for
		case 69: // problems of the small intestine
		case 70: // indicates disorders
		case 71: // conditions that may be discovered 
		case 72: // Many disorders can be diagnosed with bronchoscopy, including
		case 73: // diseases that may affect
		case 74: // may detect many diseases
		case 75: // can indicate
		case 76: // may be the result of
		case 77: // may be signs of 
		case 78: // may be also performed for
		case 79: // Other causes
		case 80: // anything abnormal was seen during the test, including
		case 81: // infections are due to
		case 82: // Benign bone tumors include
		case 83: // Cancerous tumors include
		case 84: // abnormal results are due to
		case 85: // reasons for an abnormal result
			
			for (int k=0; k<startDisordersDOM.length;k++) {
				abnormal = abnormal.replaceAll(".html", "-html");
				int startIndex = abnormal.indexOf(startDisordersDOM[k]);

				if (abnormal.indexOf(".") > -1 && abnormal.indexOf(".") < startIndex) {
					startIndex = -1;
				}

				int endIndex = -1;
				if (startIndex > -1) {
					startIndex = startIndex+startDisordersDOM[k].length();
					for (int l=0;l<endDisorderDOM.length;l++) {
						endIndex = abnormal.indexOf(endDisorderDOM[l]);
						if (endIndex > startIndex){ // done to prevent <ul> coming after </ul>
							/*DOM Format for disorders having <li> </li>*/
							disorder = disorder.concat(abnormal.substring(startIndex,endIndex));
							break;
						}	
					}

				} else {
					/* "may be due to" and after this no enumeration but a sentence */
					/* Catch using chunker Done primitive chunking NOT TO BE USED - JUST FOR DEBUGS */
					/* startIndex = 0;
						for (int l=0;l<endDisorderDOM.length;l++) {
							endIndex = abnormal.indexOf(endDisorderDOM[l]);

							if (endIndex > -1) {
								if (abnormal.indexOf(".html") > -1) {
									abnormal = abnormal.replaceAll(".html", "-html");
									l=0;
									continue;
								}
								disorder = disorder.concat(abnormal.substring(startIndex,endIndex));
								//System.out.println(abnormal.substring(startIndex,endIndex));
								break;
							}
						}*/
				}
			}
			break;
		default:
			break;
		}
		disorder = disorder.concat("</ul>");
		return disorder;
	}


	public void parseHTML(File file) {
		String str=null;
		String allText = "";
		BufferedReader br = null;

		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			while ((str = br.readLine())!=null){
				allText = allText.concat(str);
			}

			/*Get the test title*/
			if (allText.indexOf(startTitleDOM)>=0) {

				/* For each of the title we need the abnormal results data */
				if (allText.indexOf(startAbnormalDOM)>=0){
					int startIndex = allText.indexOf(startTitleDOM)+startTitleDOM.length();
					int endIndex = allText.indexOf(endTitleDOM);
					String title = allText.substring(startIndex, endIndex);
					//System.out.println(title);
					startIndex = allText.indexOf(startAbnormalDOM)+startAbnormalDOM.length();
					endIndex = allText.length();
					int index = 0;
					for (int i=0;i<endAbnormalDOM.length;i++){
						if (allText.indexOf(endAbnormalDOM[i])>=0){
							index = allText.indexOf(endAbnormalDOM[i]);
							if (endIndex>=index){
								endIndex = index;
							}
						}
					}

					String abnormalValue = allText.substring(startIndex, endIndex);
					//System.out.println(abnormalValue);
					count++;

					/* For debug purposes */
					hm.put(file.toString(), "<h4>"+title+"</h4>\n"+abnormalValue);

				}
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void writeResultTreeMapArrayList(TreeMap<String, ArrayList<String>> hm, String filename, String set) 
	{
		File newFile = new File(filename);

		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
			out.append("<h1>");
			out.append(set);
			out.append("</h1>");
			Iterator it=hm.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, ArrayList<String>> pairs = (Map.Entry<String, ArrayList<String>>)it.next();
				out.append("<h3>");
				out.append(pairs.getKey());
				out.append("</h3>");
				out.append("\n");
				out.append("<ul>");
				ArrayList<String> parts = pairs.getValue();
				for (String part:parts){
					out.append("<li>");
					out.append(part);
					out.append("</li>");
				}
				out.append("</ul>");
				out.append("\n");
				out.append("\n");
			}
			out.flush();
			out.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void writeResult(TreeMap<String, String> hm, String filename) 
	{
		File newFile = new File(filename);

		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
			Iterator it=hm.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
				out.append("<h3>");
				out.append(pairs.getKey());
				out.append("</h3>");
				out.append("\n");
				out.append(pairs.getValue());
				out.append("\n");
				out.append("\n");
			}

			out.flush();
			out.close();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void displayHashMap(TreeMap<String, String> intermediate) {
		System.out.println("Display");
		Iterator it = intermediate.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> pairs = (Entry<String, String>) it.next();
			System.out.println(pairs.getKey());
			System.out.println(pairs.getValue());
			System.out.println();
		}
	}
}
