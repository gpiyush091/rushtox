package main;

import java.util.ArrayList;

/*
import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.HirstStOnge;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.LeacockChodorow;
import edu.cmu.lti.ws4j.impl.Lesk;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
*/
import fileHandler.FileHandler;

public class Main {
	
	/*
	private static ILexicalDatabase db = new NictWordNet();
	private static RelatednessCalculator[] rcs = {
		new HirstStOnge(db),
		new LeacockChodorow(db),
		new Lesk(db),
		new WuPalmer(db),
		new Resnik(db),
		new JiangConrath(db),
		new Lin(db),
		new Path(db)
	};
	*/
	
	public static void main(String[] args) {
		
		String dirPath = "F:\\remote\\CSE599\\TestData-Rush";
		System.out.println("The Game Begins !");
		FileHandler fh = new FileHandler();
		fh.readAllFiles(dirPath);
		System.out.println("The Game Ends !");
		
		/*
		// Test ws4j sentence vs sentence
		WS4JConfiguration.getInstance();
		
		System.out.println("****************************************************");
		String sentence1 = "higher than normal levels of aldosterone";
		String sentence2 = "high levels of uric acid";
		String[] arr1 = sentence1.split(" ");
		String[] arr2 = sentence2.split(" ");
		double[][] matrix = null;
		for (RelatednessCalculator rc:rcs) {
			try {
				matrix = rc.getSimilarityMatrix(arr1, arr2);
			} catch (NullPointerException e) {
				System.out.println("Null in here"+rc);
			}
			break;
		}
		System.out.println("****************************************************");
		for (int i=0;i<matrix.length;i++) {
			for (int j=0;j<matrix[0].length;j++) {
				System.out.print(matrix[i][j]+"  ***  ");
			}
			System.out.println();
		}
		
		System.out.println("WORD WISE ****************************************************");
		for (RelatednessCalculator rc:rcs) {
			for (int i=0;i<arr1.length;i++) {
				for (int j=0;j<arr2.length;j++) {
					matrix[i][j] = rc.calcRelatednessOfWords(arr1[i], arr2[j]);
				}
			}
			break;
		}
		
		System.out.println("PRINT MATRIX ****************************************************");
		for (int i=0;i<matrix.length;i++) {
			for (int j=0;j<matrix[0].length;j++) {
				System.out.print(matrix[i][j]+"  ***  ");
			}
			System.out.println();
		}
		*/		
	}
}
