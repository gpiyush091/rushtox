<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="dataaccess.DataManager"%>
<%@page import="java.util.Collection"%>

<% Collection<JSONObject> results = DataManager.getInstance().getResults(); %>

<div class="page-header" align="center">
	<h1>Results Catalog <small>Results and relationship to tests and disorders</small></h1>
</div>
<div align="center">
	<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Results</th>
			<th>Tests</th>
			<th>Disorders</th>
		</tr>
	</thead>
	<tbody>
	<%
		for(JSONObject result:results) {
	%>
		<tr>
			<td>
			<%=result.get("text") %>
			</td>
			<td>
			<ul>
			<%
				// test list
				JSONArray testsJSON = (JSONArray)result.get("tests");
				for(int i=0;i<testsJSON.size();i++) {
				%>
					<li><%=testsJSON.get(i) %></li>
				<%}
			%>
			</ul>
			</td>
			<td>
			<ul>
			<%
				// disorder list
				JSONArray disordersJSON = (JSONArray)result.get("disorders");
				for(int i=0;i<disordersJSON.size();i++) {
				%>
					<li><%=disordersJSON.get(i) %></li>
				<%} %>
			</ul>
			</td>
		</tr>
	<%
		} 
	%></tbody>
	</table>
</div>