<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="dataaccess.DataManager"%>
<%@page import="java.util.Collection"%>

<% Collection<JSONObject> disorders = DataManager.getInstance().getDisorders(); %>

<div class="page-header" align="center">
	<h1>Disorders Catalog <small>Disorders and relationship to results and tests</small></h1>
</div>
<div>
	<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Disorders</th>
			<th>Tests</th>
			<th>Results</th>
		</tr>
	</thead>
	<tbody>
	<%
		for(JSONObject disorder:disorders) {
	%>
		<tr>
			<td>
			<%=disorder.get("text") %>
			</td>
			<td>
			<ul>
			<%
				// result list
				JSONArray testsJSON = (JSONArray)disorder.get("tests");
				for(int i=0;i<testsJSON.size();i++) {
				%>
					<li><%=testsJSON.get(i) %>
					<ul>
					<%
						JSONArray trListJSON = (JSONArray)disorder.get(testsJSON.get(i).toString());
						for(int j=0;j<trListJSON.size();j++) {
						%>
							<li><%=trListJSON.get(j) %></li>
						<%}
					%>
					</ul></li>
				<%}
			%>
			</ul>
			</td>
			<td>
			<ul>
			<%
				// disoredr list
				JSONArray resultsJSON = (JSONArray)disorder.get("results");
				for(int i=0;i<resultsJSON.size();i++) {
				%>
					<li><%=resultsJSON.get(i) %></li>
				<%} %>
			</ul>
			</td>
		</tr>
	<%
		} 
	%></tbody>
	</table>
</div>