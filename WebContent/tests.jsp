<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="dataaccess.DataManager"%>
<%@page import="java.util.Collection"%>

<% Collection<JSONObject> tests = DataManager.getInstance().getTests(); %>

<div class="page-header" align="center">
	<h1>Tests Catalog <small>Tests and relationship to results and disorders</small></h1>
</div>
<div align="center">
	<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Tests</th>
			<th>Results</th>
			<th>Disorders</th>
		</tr>
	</thead>
	<tbody>
	<%
		for(JSONObject test:tests) {
	%>
		<tr>
			<td>
				<a href="#" onCLick="loadGraph(<%=test.get("text")%>)"><%=test.get("text") %></a>
			</td>
			<td>
			<ul>
			<%
				// result list
				JSONArray resultsJSON = (JSONArray)test.get("results");
				for(int i=0;i<resultsJSON.size();i++) {
				%>
					<li><%=resultsJSON.get(i) %>
					<ul>
					<%
						JSONArray rdListJSON = (JSONArray)test.get(resultsJSON.get(i).toString());
						for(int j=0;j<rdListJSON.size();j++) {
						%>
							<li><%=rdListJSON.get(j) %></li>
						<%}
					%>
					</ul></li>
				<%}
			%>
			</ul>
			</td>
			<td>
			<ul>
			<%
				// disoredr list
				JSONArray disordersJSON = (JSONArray)test.get("disorders");
				for(int i=0;i<disordersJSON.size();i++) {
				%>
					<li><%=disordersJSON.get(i) %></li>
				<%} %>
			</ul>
			</td>
		</tr>
	<%
		} 
	%></tbody>
	</table>
</div>

<script type="text/javascript">
	
</script>