<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>RushTox</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/d3.v3.min.js"></script>
<script type="text/javascript">

</script>

<style>
.navbar-default .navbar-brand {
	color:#ffffff;
}

.navbar-default .navbar-brand:hover, .navbar-default .navbar-brand:focus {
	color:#E78181;
}

.navbar-default {
	background-color: #A01717;
}

.navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:hover, .navbar-default .navbar-nav>.active>a:focus {
	color:#E78181;
	background-color: #A01717;
}

.navbar-default .navbar-nav>li>a {
	color:#ffffff;
}

.navbar-default .navbar-nav>li>a:hover, .navbar-default .navbar-nav>li>a:focus {
	color:#E78181;
	background-color: #A01717;
}

.navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:hover, .navbar-default .navbar-nav>.open>a:focus {
	color:#E78181;
	background-color: #A01717;
}

th {
	text-align: center;
}

td {
	text-align: left;
}

tbody {
	align: center;
}

.table {
	width: 100%;
	margin-bottom: 20px;
	margin-right: 20px;
	margin-top: 20px;
}

</style>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">CroXide</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#" class="navbar-links">Home</a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Nodes<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#" onClick="return loadTests();">Tests</a></li>
            <li><a href="#" onClick="return loadResults();">Results</a></li>
            <li><a href="#" onClick="return loadDisorders();">Disorders</a></li>
          </ul>
        </li>
        <li><a href="#" onClick="return loadGraph();">Graph</a></li>
      </ul>
      <!-- <form class="navbar-form navbar-left" role="search" action="RushToxApi" method="get">
        <button type="submit" class="btn btn-default">Visualize</button>
      </form> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">About</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div id="content">

</div>

<script type="text/javascript">
	function loadTests() {
		jQuery.ajax({
			type:'get',
			url:'tests.jsp',
			success: function(data) {
				$('#content').empty().html(data);
			}
		});
	}
	
	function loadResults() {
		jQuery.ajax({
			type:'get',
			url:'results.jsp',
			success: function(data) {
				$('#content').empty().html(data);
			}
		});
	}
	
	function loadDisorders() {
		jQuery.ajax({
			type:'get',
			url:'disorders.jsp',
			success: function(data) {
				$('#content').empty().html(data);
			}
		});
	}
	
	function loadGraph() {
		jQuery.ajax({
			type:'get',
			url:'graph.jsp',
			success: function(data) {
				$('#content').empty().html(data);
			}
		});
	}
	
</script>

</body>
</html>