<%@page import="dataaccess.DataManager"%>
<%@page import="org.json.simple.JSONObject"%>

<% JSONObject linksJSON = DataManager.getInstance().getLinksForGraph();%>

<div id="graph">
</div>

<style>
.link {
  stroke: #ccc;
}

.node text {
  pointer-events: none;
  font: 10px sans-serif;
}
</style>

<script type="text/javascript">
	var graph=<%=linksJSON%>
	
	var width = 2000,
	    height = <%=DataManager.getInstance().getHeight()%>;

	var color = d3.scale.category20();
	
	var svg = d3.select("#graph").append("svg")
    	.attr("width", width)
    	.attr("height", height);
	
	var force = d3.layout.force()
	    .gravity(.05)
	    .distance(100)
	    .charge(-100)
	    .size([width, height]);
	
	
  	force.nodes(graph.nodes)
      	.links(graph.links)
      	.start();
  		
  	var link = svg.selectAll(".link")
    	  	.data(graph.links)
			.enter().append("line")
      		.attr("class", "link")
      		.style("stroke", function(d) {
      			return d.stroke;
      		})
  			.style("stroke-width", function(d) { return Math.sqrt(d.value); });
  	
  	var node = svg.selectAll(".node")
      	.data(graph.nodes)
    	.enter().append("g")
      	.attr("class", "node");
      
  	node.append("circle")
        .attr("r", function (d) {
        	if (d.group == 0 || d.group == 2) {
        		return 10;
        	} else if (d.group == 1){
        		return 20;
        	}
        	return 5;
        });
    
  	node.select("circle")
    	.style("fill", function(d) { return color(d.group); });
  	
   	node.append("text")
      	.attr("dx", 12)
      	.attr("dy", ".35em")
      	.text(function(d) { return d.name; });
   	
   	node.on("click", function(d) {
   	    // loadSubset(d.name, d.group);
   	});
	
  	force.on("tick", function() {
    	link.attr("x1", function(d) { return d.source.x; })
        	.attr("y1", function(d) { return d.source.y; })
        	.attr("x2", function(d) { return d.target.x; })
        	.attr("y2", function(d) { return d.target.y; });

    	node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  	});
	
  	function loadSubset(name, group) {
  		$.post("subset.jsp", {'nodeName': name, 'nodeGroup': group}, 
  		    function(data) 
  		    { 
  				// $('#content').empty().html(data);
  		        //alert("Result from second.jsp: " + data.nodeName + " " + data.nodeGroup); 
  			}
  		);
  	}
</script>